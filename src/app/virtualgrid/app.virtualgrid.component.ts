import { Component } from '@angular/core';
import { EmpDataService } from 'src/app/common/app.common.services';
import { IEmployee } from 'src/app/common/IEmployee';
import { IEmployeeSubmit } from 'src/app/common/IEmployeeSubmit';

@Component({
    selector: 'app-virtual-grid',
    templateUrl: 'app.virtualgrid.component.html',
    styleUrls: ['app.virtualgrid.component.css'],
    providers: [EmpDataService]
})

export class VirtualEmpGridComponent {

    employees: IEmployee[];
    employeeKeys: string[];

    throttle = 300;
    scrollDistance = 1;
    scrollUpDistance = 2;
    isScrollUp = false;
    isScrollDown = false;

    constructor(private empDataService: EmpDataService) {
        this.initGridData();
    }

    // Function to call service method to read JSON data
    initGridData() {
        console.log('initGridData Called');
        this.empDataService.getJSON()
            .subscribe(
            // The 1st callback handles the data emitted by the observable.
            (results) => {
                if (this.isScrollDown) {
                    Array.prototype.push.apply(this.employees, results);
                    console.log('isScrollDown');
                } else if (this.isScrollUp) {
                    Array.prototype.unshift.apply(this.employees, results);
                    console.log('isScrollUp');
                } else {
                    this.employees = results;
                    this.employeeKeys = Object.keys(this.employees[0]);
                }
            },
            // The 2nd callback handles errors.
            (err) => console.error(err),
            // The 3rd callback handles the "complete" event.
            () => console.log('observable complete')
            );
    }

    onScrollDown(ev) {
        console.log('scrolled down!!', ev);
        this.isScrollUp = true;
        this.isScrollDown = false;
        this.initGridData();
    }

    onUp(ev) {
        console.log('scrolled up!', ev);
        this.isScrollUp = false;
        this.isScrollDown = true;
        this.initGridData();
    }
}
