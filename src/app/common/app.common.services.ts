import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IEmployee } from 'src/app/common/IEmployee';
import { IEmployeeSubmit } from 'src/app/common/IEmployeeSubmit';

@Injectable()
export class EmpDataService {
    private jsonFileURL = 'assets/empData.json';
    private submitApiURL = 'api/submit';
    private data: any;
    constructor(private http: HttpClient) {

    }

    // Function to read the JSON data
    getJSON(): Observable<IEmployee[]> {
        return this.http.get<IEmployee[]>(this.jsonFileURL).pipe(
            catchError(this.handleError)
        );
    }

    // Function to read the JSON data
    getEmployees(page: number = 1): Observable<IEmployee[]> {
        return this.http.get<IEmployee[]>(this.jsonFileURL).pipe(
            catchError(this.handleError)
        );
    }

    // Function to submit the values to api
    submitEmpData(empData: IEmployeeSubmit): Observable<IEmployeeSubmit> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };

        return this.http.post<IEmployeeSubmit>(this.submitApiURL, empData, httpOptions).pipe(
            catchError(this.handleError)
        );
    }

    // Function to handle Error
    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }
}
