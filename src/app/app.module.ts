import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { GridComponent } from './grid/app.grid.component';
import { VirtualEmpGridComponent } from './virtualgrid/app.virtualgrid.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

const appRoutes: Routes = [
  { path: 'grid', component: GridComponent },
  { path: 'virtualgrid', component: VirtualEmpGridComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    GridComponent,
    VirtualEmpGridComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    InfiniteScrollModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
