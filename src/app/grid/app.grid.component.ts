import { Component, OnInit } from '@angular/core';
import { EmpDataService } from 'src/app/common/app.common.services';
import { IEmployee } from 'src/app/common/IEmployee';
import { IEmployeeSubmit } from 'src/app/common/IEmployeeSubmit';
import * as $ from 'jquery';

@Component({
    selector: 'app-grid',
    templateUrl: 'app.grid.component.html',
    styleUrls: ['app.grid.component.css'],
    providers: [EmpDataService]
})

export class GridComponent implements OnInit {
    employees: IEmployee[];
    employeeKeys: string[];
    filteredItems: IEmployee[];
    currentIndex = 1;
    pageStart = 1;
    pages = 5;
    pageSize = 10;
    pageNumber = 0;
    pagesIndex: Array<number>;
    rowsperpage = 0;


    constructor(private empDataService: EmpDataService) {
    }

    ngOnInit() {
        this.initGridData();
        this.setScroll();
    }

    // Function to call service method to read JSON data
    initGridData() {
        console.log('initGridData Called');
        this.empDataService.getJSON()
            .subscribe(
            // The 1st callback handles the data emitted by the observable.
            (results) => {
                this.filteredItems = results;
                this.init();
            },
            // The 2nd callback handles errors.
            (err) => console.error(err),
            // The 3rd callback handles the "complete" event.
            () => console.log('observable complete')
            );
    }

    // Function to submit the Emp Data
    // params : rowID, rowStatus
    submitEmpData(rowID: number, rowStatus: string) {
        const empData: IEmployeeSubmit = {
            rowID: rowID,
            rowStatus: rowStatus
        };
        console.log('submit Post click happend ', empData);

        this.empDataService.submitEmpData(empData)
            .subscribe();
    }

    // Function called on changing the Rows Per Page count
    filterGrid(count: string) {
        console.log('count ' + count);
        let rowCount = 0;
        if (count !== '') {
            rowCount = parseInt(count, 10);
            this.rowsperpage = rowCount;
        } else {
            // If rows per page is blank
            this.rowsperpage = 0;
        }

        this.init();
    }

    // Function to initialize the grid & pagination
    init() {
        this.currentIndex = 1;
        this.pageStart = 1;
        this.pages = 5;
        if (this.rowsperpage > 0) {
            this.pageSize = this.rowsperpage;
        } else {
            this.pageSize = 10;
        }
        this.pageNumber = parseInt('' + (this.filteredItems.length / this.pageSize), 10);
        if (this.filteredItems.length % this.pageSize !== 0) {
            this.pageNumber++;
        }

        if (this.pageNumber < this.pages) {
            this.pages = this.pageNumber;
        }
        console.log('init', 'this.filteredItems.length  : ' + this.filteredItems.length +
            ' this.pageNumber :  ' + this.pageNumber + ' this.pageSize : ' + this.pageSize + ' this.pages : ' + this.pages);

        this.refreshItems();
    }

    // Function to set the paging array
    fillArray(): any {
        console.log('fillArray', 'this.pageStart : ' + this.pageStart + ' this.pages :' + this.pages);
        const obj = new Array();
        for (let index = this.pageStart; index < this.pageStart + this.pages; index++) {
            obj.push(index);
        }
        return obj;
    }

    // Function to refresh the grid
    refreshItems() {
        console.log('refreshItems ', 'this.filteredItems.length  : ' + this.filteredItems.length
            + 'this.rowsperpage : ' + this.rowsperpage
            + ' this.currentIndex : ' + this.currentIndex + ' this.pageSize :'
            + this.pageSize + ' this.pages' + this.pages);

        this.employees = this.filteredItems.slice((this.currentIndex - 1) * this.pageSize, (this.currentIndex) * this.pageSize);

        this.employeeKeys = Object.keys(this.employees[0]);

        this.pagesIndex = this.fillArray();
    }

    // Previous page click function
    prevPage() {
        console.log('prevPage');
        if (this.currentIndex > 1) {
            this.currentIndex--;
        }
        if (this.currentIndex < this.pageStart) {
            this.pageStart = this.currentIndex;
        }
        this.refreshItems();
    }

    // Next page click function
    nextPage() {
        console.log('nextPage', this.currentIndex, this.pageNumber, this.pageStart, this.pages);
        if (this.currentIndex < this.pageNumber) {
            this.currentIndex++;
        }
        if (this.currentIndex >= (this.pageStart + this.pages)) {
            this.pageStart = this.currentIndex - this.pages + 1;
        }

        this.refreshItems();
    }

    // Function to set the current Index on click of page numbers
    setPage(index: number) {
        console.log('setPage', index);
        this.currentIndex = index;
        this.refreshItems();
    }

    setScroll() {
        $('table').on('scroll', function () {
            $('table > *').width($('table').width() + $('table').scrollLeft());
        });
    }
}
